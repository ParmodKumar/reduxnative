import {createStore, compose} from 'redux';
import rootReducers from './src/redux/Reducer/index';

let composeEnhancers = compose;

if (__DEV__) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}
const store = createStore(rootReducers, composeEnhancers());
export default store;
