import chanNumbar from '../Reducer/updown';
import {combineReducers} from 'redux';
import Sinup from './authentication';
const rootReducers = combineReducers({
  chanNumbar,
  Sinup,
});

export default rootReducers;
