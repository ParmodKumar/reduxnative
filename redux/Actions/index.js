export const incNumbar = quantity => {
  return {
    type: 'INCRMENT',
    quantity: quantity,
  };
};
export const decNumbar = quantity => {
  return {
    type: 'DECRMENT',
    quantity: quantity,
  };
};

export const Sinup = user => {
  return {
    type: 'SINUP',
    payload: user,
  };
};
